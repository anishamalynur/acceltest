﻿
using System;
using Foundation;
using CoreMotion;
using UIKit;
using MessageUI;
using System.Collections.Generic;
namespace Accel
{

    public partial class ViewController : UIViewController
    {
        double prevX = 0;
        double prevY = 0;
        double prevZ = 0;

        bool startCollecting = false;

        List<double[]> accelerationData = new List<double[]>();

        CMMotionManager motionManager = new CMMotionManager();
        MFMailComposeViewController mailController;

        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            TranslateButton.TouchUpInside += (object sender, EventArgs e) => {

                if (motionManager.AccelerometerAvailable)
                {
                    motionManager.AccelerometerUpdateInterval = .01;

                    motionManager.StartAccelerometerUpdates(NSOperationQueue.CurrentQueue, (data, error) =>
                    {
                        startCollecting = true;
                        this.labelX.Text = data.Acceleration.X.ToString();
                        this.labelY.Text = data.Acceleration.Y.ToString();
                        this.labelZ.Text = data.Acceleration.Z.ToString();
                        collectData(data);
                    });
                }

            };

            CallButton.TouchUpInside += (object sender, EventArgs e) => {
                // Use URL handler with tel: prefix to invoke Apple's Phone app...
                //var url = new NSUrl("tel:" + translatedNumber);
                startCollecting = false;

                if (MFMailComposeViewController.CanSendMail)
                {

                    var to = new string[] { "anisha.malynur@gmail.com" };
                    string output = getAccelerationDataAsString();

                    if (MFMailComposeViewController.CanSendMail)
                    {
                        mailController = new MFMailComposeViewController();
                        mailController.SetToRecipients(to);
                        mailController.SetSubject("accel data :)");
                        mailController.SetMessageBody(output, false);
                        mailController.Finished += (object s, MFComposeResultEventArgs args) => {

                            Console.WriteLine("result: " + args.Result.ToString()); // sent or cancelled

                            BeginInvokeOnMainThread(() => {
                                args.Controller.DismissViewController(true, null);
                            });
                        };
                    }

                    this.PresentViewController(mailController, true, null);
                }
                else
                {
                    new UIAlertView("Mail not supported", "Can't send mail from this device", null, "OK");
                }


                // ...otherwise show an alert dialog
                /* if (!UIApplication.SharedApplication.OpenUrl(url))
                 {
                     var alert = UIAlertController.Create("Not supported", "Scheme 'tel:' is not supported on this device", UIAlertControllerStyle.Alert);
                     alert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));
                     PresentViewController(alert, true, null);
                 }*/
            };
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        private string getAccelerationDataAsString()
        {
            string accelerationString = "";
            foreach (double[] point in accelerationData)
            {
                string currentPointString = point[0] + "," + point[1] + "," + point[2] + "\n";
                accelerationString += currentPointString;
            }
            return accelerationString;
        }

        public void collectData(CMAccelerometerData data)
        {
            //todo put into datastructure for later export

            if (startCollecting)
            {
                //todo then put into datastructure because start button has been clicked
                double[] dataPoint = new double[3];
                //Console.WriteLine(dataCount);
                //ds.accelData[dataCount, 0] = data.Acceleration.X;
                //ds.accelData[dataCount, 1] = data.Acceleration.Y;
                //ds.accelData[dataCount, 2] = data.Acceleration.Z;
                double diffX = data.Acceleration.X - prevX;
                double diffY = data.Acceleration.Y - prevY;
                double diffZ = data.Acceleration.Z - prevZ;
                if (diffX > .2 || diffY > .2 || diffZ > .2)
                {
                    dataPoint[0] = data.Acceleration.X;
                    dataPoint[1] = data.Acceleration.Y;
                    dataPoint[2] = data.Acceleration.Z;

                    prevX = data.Acceleration.X;
                    prevY = data.Acceleration.Y;
                    prevZ = data.Acceleration.Z;

                    accelerationData.Add(dataPoint);

                }


                // dataCount += 1;

            }
        }
    }
}
